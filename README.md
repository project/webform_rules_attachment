Webform rules attachment, is an addon for the [webform
rules](http://drupal.org/project/webform_rules) and
[mimemail](http://drupal.org/project/mimemail). With this module you
will be able to attach webform file fields and the complete webform submission as csv
file to the generated email.

# INSTALLATION
Download module and enable from admin/build/modules.

# USAGE
* Create a webform with a bunch of components.
* Create a triggered rule, which sends an email to some kind of a user.
* In **Token replacement patterns** > **Replacement patterns for The submitted
 webform data** you can se a few new tokens.
  * [data:submitted-values]
  * [data:{component}-file-mimetype]
  * [data:{component}-file-path]
  These new fields is available at the **attachments** field in the very buttom of
  the triggered rule page.

An example of how to add these tokens.

    text/csv:[data:submitted-values]
    [data:file-file-mimetype]:[data:file-file-path]
    [data:file_2-file-mimetype]:[data:file_2-file-path]

The `file` and `file_2` is the field key from the webform component edit page.

# TODO
* Support for Drupal 7

# CREDITS
Module is developed at [Reload!](http://reload.dk) and sponsored by
[FDB.dk](http://fdb.dk).
